import { useDispatch } from 'react-redux';
import './App.scss';
import { LoginState } from './stateManagement/loginReducer';
import { RegState } from './stateManagement/regReducer';

function App() {
  
  const dispatch = useDispatch();
  
  const Clear = () => {
    dispatch({
        type: LoginState.CLEAR,
    });
    dispatch({
        type: RegState.CLEAR,
    });
}

  return (
  <div>Main app
  <div className="button" onClick={Clear}>Clear</div>
  </div>
  );
}

export default App;