
interface State {
    list: string[];
}

const initialState: State = {
    list: [],
}

export const LoginState = Object.freeze({
    ADD: 'LOGIN_ACTION_ADD',
    CLEAR: 'LOGIN_ACTION_CLEAR',
});

type Action = { type: string, payload?: any };

const loginReducer = (state = initialState, action: Action) => {
    switch (action.type) {
        case LoginState.ADD:
            const list = [...state.list];
            
            const item = action.payload;
            
            list.push(item);

            const newState = { ...state, list: [...list] };
            
            return newState;

        case LoginState.CLEAR:
            return { ...state, list: [] };
        default:
            return state;
    }
};
export default loginReducer;