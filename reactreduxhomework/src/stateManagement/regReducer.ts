
interface State {
    list: string[];
}

const initialState: State = {
    list: [],
}

export const RegState = Object.freeze({
    ADD: 'REG_ACTION_ADD',
    CLEAR: 'REG_ACTION_CLEAR',
});

type Action = { type: string, payload?: any };

const regReducer = (state = initialState, action: Action) => {
    switch (action.type) {
        case RegState.ADD:
            const list = [...state.list];
            
            const item = action.payload;
            
            list.push(item);

            const newState = { list: [...list] };
            
            return newState;

        case RegState.CLEAR:
            return { list: [] };
        default:
            return state;
    }
};
export default regReducer;