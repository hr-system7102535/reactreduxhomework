import React from "react";
import { useDispatch } from "react-redux";
import { RegState } from "../stateManagement/regReducer";

function Registration() {
    const [text, setText] = React.useState("")

    const dispatch = useDispatch();

    const Save = () => {
        dispatch({
            type: RegState.ADD,
            payload: text,
        });
        setText("");
    }

    const Clear = () => {
        dispatch({
            type: RegState.CLEAR,
        });
    }

    return (
        <div>   
            REGISTRATION         
            <input type="text" placeholder="text" onChange={e => setText(e.target.value)} value={text}/>
            <div className="button" onClick={Save}>Save</div>
            <div className="button" onClick={Clear}>Clear</div>
        </div>
    );
  }
  export default Registration;