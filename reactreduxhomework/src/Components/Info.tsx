import { useSelector } from "react-redux";
import InfoItem, { InfoItemWithColor } from "./InfoItem";

function InfoScreen() {

    const loginList = useSelector((x: {
        login: any
    }) => x.login.list);
    const regList = useSelector((x: {
        registration: any
    }) => x.registration.list);

    const infoWithColor = InfoItemWithColor(InfoItem);

    function getInfo(x: string, color: string){
        return infoWithColor({color:color, val:x});
    }

    return (
        <div className="info">
            <div className="infoColumn">
                {loginList.map((x:string) => getInfo(x, "blue"))}
            </div>
            <div className="infoColumn">
                {regList.map((x:string) => getInfo(x, "red"))}
            </div>
        </div>
    )
};

export default InfoScreen;


