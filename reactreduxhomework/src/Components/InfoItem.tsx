import { Component, Key, ReactNode } from "react";

interface Props {
    val: string;
}

interface ColorProps {
    color: string;
}

export const InfoItemWithColor = <P extends object>(Com: React.ComponentType<P>) =>
    (props: ColorProps & P) => {
        return (
            <div className={props.color} key={`oi_${Math.random()}`}>
                <Com {...props}/>
            </div>
        )
    }

class InfoItem extends Component<Props> {    
    render(): ReactNode {
    return (
        <div className="infoItem">
            {this.props.val}
        </div>
    )}
}

export default InfoItem;