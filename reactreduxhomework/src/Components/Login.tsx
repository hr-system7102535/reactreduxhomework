import React from "react";
import { LoginState } from "../stateManagement/loginReducer";
import { useDispatch } from 'react-redux';

function Login() {
    const [text, setText] = React.useState("")

    const dispatch = useDispatch();

    const Save = () => {
        dispatch({
            type: LoginState.ADD,
            payload: text,
        });
        setText("");
    }

    const Clear = () => {
        dispatch({
            type: LoginState.CLEAR,
        });
    }

    return (
        <div>            
            LOGIN
            <input type="text" placeholder="text" onChange={e => setText(e.target.value)} value={text}/>
            <div className="button" onClick={Save}>Save</div>
            <div className="button" onClick={Clear}>Clear</div>
        </div>
    );
}

export default Login;