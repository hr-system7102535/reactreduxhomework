import { configureStore } from '@reduxjs/toolkit';
import loginReducer from './stateManagement/loginReducer';
import regReducer from './stateManagement/regReducer';

const store= configureStore({
    reducer: {
        login: loginReducer,
        registration: regReducer,
    },
   
});
export default store;