
import ReactDOM from 'react-dom/client';
import './index.css';
import reportWebVitals from './reportWebVitals';
import { BrowserRouter, Link, Route, Routes } from 'react-router-dom';
import App from './App';
import Registration from './Components/Registration';
import NotFound from './Components/NotFound';
import Login from './Components/Login';
import InfoScreen from './Components/Info';
import { Provider } from 'react-redux';
import store from './store';


const root = ReactDOM.createRoot(
  document.getElementById('root') as HTMLElement
);

root.render(

  <Provider store={store}>
    <BrowserRouter>
      <div className='header'>
        <Link to={'/'}>Home</Link>
        <Link to={'/Login'}>Login</Link>
        <Link to={'/Registration'}>Registration</Link>
      </div>
      <div className='reduxContainer'>
        <div className='menuContainer'>
          <Routes>
            <Route index element={<App />} />
            <Route path="Login" element={<Login />} />
            <Route path="Registration" element={<Registration />} />
            <Route path="*" element={<NotFound />} />
          </Routes>
        </div>
        <div className='separator'></div>
        <div className='infoscreen'>
          <InfoScreen />
        </div>
      </div>
    </BrowserRouter>
  </Provider>
);

reportWebVitals(); 